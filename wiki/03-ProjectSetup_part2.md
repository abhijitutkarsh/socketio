Now we will create a server using express and then connect it to the socket.io framework.

Create a file named **`app.js`** to connect the socket to the server using the following command into the terminal:

```js
touch app.js
```

We have already learnt to create a server using express. Consider the following code which creates the server. This code will be written in **`app.js`** file:

```js
const express = require('express')
const app = express();
const http = require('http').Server(app)


//set the template engine ejs
app.set('view engine', 'ejs')

//path to find the static content
app.use(express.static('public'))


//Routing function which denotes that whenever there is get request on the server,
//it must send the index.ejs file(which we will be creating later) in response.
app.get('/', (req, res) => {
	res.render('index')
})

//Listen on port 3000
server = http.listen(3000);
```
The above code is normally used to create a server and routing a file on a path specified.

Now we need to create a chat application where each user sends a message which is received by the server and which in-turn broadcasts it to all the other clients connected to the server.

As explained before, the socket helps in bi-directional communication between a client and a server. First, we will connect the socket.io to our server. We will use a socket such that whenever a new client gets connected to the server, the server must respond.

To connect the socket.io package, we must install it using the following command:

```js
npm install --save socketio
```

All the following code will also be written in the **`app.js`** file created above.

Now the first step is to require the **`socket.io`** package and obtain its object. 

After writing above code snippet, write the following command:
```js
const io = require("socket.io")(server);
```

We need a bi-directional communication among the server and the client, thus the following function helps us to do so. Consider the following code:

```js
io.on('connection', (socket) => {
	console.log('New user connected');
})
```
- We have used a function **`io.on`** which takes in two arguments: the first argument denotes any event to which the server must respond. The event may be pre-defined and custom events. The above used is "connection" event which is predefined which means that as soon as the client gets connected the server must respond.
- The second argument is the callback function which needs to be performed in response to the event occurred.

Run the above code using the following command:

```js
node app.js
```

**Note**: `Don't forget to export the server instance that you have created above in app.js file, in given pattern`
```js
// Export server instance
module.exports={
    server: server
}
```
The above program shows the following output:
![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Nodejs+/Screenshot(2).png)

Whenever a new client is connected, it prints "New User connected" on the console.

The client response page looks as follows:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Nodejs+/client1.png)
