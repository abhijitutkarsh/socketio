# Creating a final Chatbox:

Now we will make some changes in the **`app.js`** and **`index.html`** so that the server may receive messages and also it can broadcast it to all the clients connected to it.

Let us first modify our **`app.js`**:

Consider the following code:

```js
//listen on every connection
io.on('connect', (socket) => {               //Line1
	console.log('New user connected')

    //listen on new_message
    socket.on('new_message', (data) => {        //Line2
        //broadcast the new message
        io.sockets.emit('new_message', data);   //Line3
    })  
})
```

- The _Line1_, in the above code, is a function we have created earlier which will print "New user connected" whenever a new client gets connected. We had already created this function in previous articles.

- In the same function `io.on` we will create another function that will respond whenever a new message is sent or received.
It takes 2 arguments:
    - The first argument is the `event` which is to be handled. In the above code, we have made a custom event named `new_message` which means whenever a new message is received by the server, it must respond to the client.
    - The second argument is the callback function which needs to be performed when the `event` specified above actually occurs. In the above code, the callback function is `io.sockets.emit()`. This function is defined to send the data to all the clients which are connected.

Thus the above changes in the `app.js` file will configure the socket to receive the messages and then respond accordingly.

Now we will modify our `index.ejs` file which will show all the chats for the clients.

```html
<!DOCTYPE html>
<!------------------ html and css code ----------->
<html>
  <head>
    <meta http-equiv="Content-Type" const="text/html;charset=UTF-8" />
    <link href="http://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="style.css" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <title>Simple Chat App</title>
  </head>

  <body>
    <header>
      <h1>Super Chat</h1>
    </header>

    <section>
        <div id="change_username">
          	<input id="username" type="text" />
          	<button id="send_username" type="button">Change username</button>
        </div>
    </section>

    <section id="chatroom">
        <section id="feedback"></section>
    </section>

    

    <section id="input_zone"> 
        <input id="message" class="vertical-align" type="text" />
        <button id="send_message" class="vertical-align" type="button">Send</button>
    </section>

    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/socket.io/socket.io.js"></script>
    
    <!---------- Client side socket connection ----------->
    <script>
      $(function(){
        // initiate connection
        var socket = io.connect();

        // Emit message
        $("#send_message").click(function(){
          socket.emit('new_message', $('#message').val())
        });      //Line1

        socket.on("new_message", (data) => {
          $('#feedback').html('');
          $('#message').val('');
          $('#chatroom').append("<p class='message'>" + "Anonymous["+socket.id+"]" + ": " + data + "</p>")
        })     //Line2
      });
    </script>

  </body>
</html>
```

The above code contains a section called `client-side socket connection`. This section contains some `jquery` code which you would be able to understand as soon as you learn the `jquery` course in the Frontend part.

We will just explain what all is happening through this code. 
- In _Line1_, whenever `send_message` is clicked, a function needs to be performed which is **`socket.emit()`**. `socket.emit` allows you to send custom events on the server and client. In this example, the socket.emit() takes an event called "chat_message" and passes a value to the socket which is the _actual message_ sent by the client.
- In _Line2_, we have created another function, `socket.on` which will respond every time the event given in the first argument occurs. In the above example, whenever, "chat_message" event occurs, it performs the function specified in the second argument. The callback function specified in the above example will render the index.html page with the new message appended. 
