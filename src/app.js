
const express=require("express");
const path=require("path");
const app=express();
const http=require("http").Server(app);

app.get('/',(req,res)=>
{
    var pathToIndex=path.join(__dirname,"/index.html");
    
    res.sendFile(pathToIndex);
});


server=http.listen(3000);
const io=require("socket.io")(server);

io.on("connection",(socket)=>
{
    console.log("user connected");
    socket.on('new_message',(data)=>
    {
        io.sockets.emit('new_message',data);
    });
});


module.exports={
    server: server
    }